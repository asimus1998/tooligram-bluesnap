var request = require('request');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// set the view engine to ejs
app.set('view engine', 'ejs');


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// index page
app.get('/', function(req, res) {
    res.render('pages/index');
});

app.post('/pay', function(req, res){
    var encryptedCvv = req.body.cvv,
        encryptedCreditCard = req.body.creditCard,
        CardYear =  req.body['exp-year'],
        CardMonth =  req.body['exp-month'],
        amount = 49;


    var url = 'https://sandbox.bluesnap.com/services/2/recurring/ondemand';

    var username = "API_15295927571891659182741",
        password = "Tooligram0682";
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

    console.log('Auth', auth);

    var data = {
        amount: amount,
        currency: 'USD',
        payerInfo: {
            firstName: 'John',
            lastName: 'Doe'
        },
        paymentSource: {
            creditCardInfo: {
                creditCard: {
                    expirationYear: CardYear,
                    securityCode: encryptedCvv,
                    expirationMonth: CardMonth,
                    cardNumber: encryptedCreditCard
                }
            }
        }
    };

    var options = {
        uri: url,
        method: 'POST',
        headers : {
            "Authorization" : auth
        },
        body: data,
        json: true
    };

    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            //Успешная оплата и списывание денег
            console.log(body.subscriptionId); //
            return res.send(JSON.stringify({
                success: true,
                msg: 'Успешная оплата'
            }));
        }else{
            //Ошибка валидации данных карты
            return res.send(JSON.stringify(body));
        }
    });
});


app.listen(8001);
console.log('8001 is the magic port');